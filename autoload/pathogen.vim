if has("win32")
    :so ~/vimfiles/bundle/vim-pathogen/autoload/pathogen.vim
else
    :so ~/.vim/bundle/vim-pathogen/autoload/pathogen.vim
endif
